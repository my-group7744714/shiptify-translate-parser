import fs from "fs";
import * as cheerio from "cheerio";
import { checkCaseType } from "./helper.js";

const parseTextWithoutQuotes = (htmlContent) => {
  const regex = />([^<{]+)</g;

  let match;
  let texts = [];

  while ((match = regex.exec(htmlContent)) !== null) {
    const text = match[1].trim();
    const testText = !/{{.*}}/.test(text);
    const testIncludes = !/ng-|[-+]|--->|\.|«|»|\/&gt;|→|\/|,|—|\||&gt;|:|;|%/.test(text);
    // Убедитесь, что текст не заключен в фигурные скобки
    if (testText && text.length && testIncludes) {
      texts.push(text);
    }
  }
  return texts;
};
export const parseHtmlFile = (filePath, jsonContent) => {
  let htmlContent = fs.readFileSync(filePath, 'utf8');
  const $ = cheerio.load(htmlContent);
  const keys = []
  const variable = []
  const ngBind = []
  let onlyString = []

  $('body').each((i, el) => {
    const bodyHtml = $(el).html();
    // const regexTranslate = /\{\{(?:'([^']*)'|"([^"]*)"|([^'"\|]*))\s*\|\s*translate\s*\}\}/g;
    const regexCombined = /(\{\{(?:'([^']*)'|"([^"]*)"|([^'"\|]*))\s*\|\s*translate\s*\}\})|(ng-bind\s*=\s*['"]([^'"]*)\s*\|\s*translate['"])/g;

    let match;

    const parseTexts = parseTextWithoutQuotes(bodyHtml)

    if (parseTexts.length) {
      onlyString = [...onlyString, ...parseTexts]

      // TODO: вынести в функицию
      const textExceptions = [
        'kg',
        'b'
      ]
      // Замена найденных строк в контенте
      // parseTexts.forEach(text => {
      //   if (textExceptions.includes(text)) return
      //
      //   const replacePattern = `>{{'${text}' | translate}}<`;
      //   htmlContent = htmlContent.replace(new RegExp(`>${text}<`, 'g'), replacePattern);
      // });
    }

    while ((match = regexCombined.exec(bodyHtml)) !== null) {
      const contentRegex = /'(.*?)'/;

      if (match[1]) {
        const newKey = match[2] || match[3] || match[4];

        if (!jsonContent.hasOwnProperty(newKey)) {
          if (checkCaseType(newKey.trim())) {
            variable.push(match[0]);

            return;
          }

          if (!newKey.includes('.')) {
            keys.push(newKey);
          }
        }
      } else if (match[5]) {
        // Обработка ng-bind="'...' | translate"
        const ngBindKey = match[6];
        if (!jsonContent.hasOwnProperty(ngBindKey)) {
          // Добавьте newKey в keys или variable, как вам нужно
          ngBind.push(ngBindKey);
        }
      }
    }
  });

  // console.log(onlyString)
  // После всех замен переписываем файл
  fs.writeFileSync(filePath, htmlContent, 'utf8');
  return { keys, variable, ngBind, onlyString };
};

export const parseJsFile = (filePath, jsonContent) => {
  const jsContent = fs.readFileSync(filePath, 'utf8');
  const regexTranslate = /translate\s*\(\s*['"]([^'"]+)['"]\s*\)/g;
  const keys = [];

  let match;

  while ((match = regexTranslate.exec(jsContent)) !== null) {
    const key = match[1] || match[2] || match[3];

    if (!jsonContent.hasOwnProperty(key)) {
      if (!key.includes('.')) {
        keys.push(key);
      }
    }
  }

  return keys;
};
