import fs from 'fs';
import dotenv from 'dotenv';
import { parseHtmlFile, parseJsFile } from "./src/parserHTML.js";
import { findHtmlFiles, findJsFiles } from "./src/findFiles.js";

dotenv.config();

const {
  INPUT_FOLDER_PATH,
  JSON_FILE_PATH,
  OUTPUT_JSON_FILE_PATH,
} = process.env

function main() {
  const START_JSON_OBJECT = JSON.parse(fs.readFileSync(JSON_FILE_PATH, 'utf8'));
  let htmlKeys = [];
  let htmlVar = [];
  let htmlNgBind = [];
  let htmlOnlyString = [];
  let jsKeys = [];

  const findHTMLCallback = filePath => {
    // Разбить на несколько регулярок каждая для конкретного случая
    // Не обрабатывает этот кейс No drivers founded. Please посмотреть почему
    //                        <div class="c-modal-table__placeholder-type">
    //                             {{ 'No drivers founded. Please' | translate }}
    //                             <a class="h-typo-link" ng-click="ctrl.addNewDriver()">{{ 'create a new one' | translate }}</a>.
    //                         </div>
    const { keys, variable, ngBind, onlyString } = parseHtmlFile(filePath, START_JSON_OBJECT)
    htmlKeys = [...htmlKeys, ...keys];
    htmlVar = [...htmlVar, ...variable];
    htmlNgBind = [...htmlNgBind, ...ngBind];
    htmlOnlyString = [...htmlOnlyString, ...onlyString];
  };
  const findJSCallback = filePath => {
    const parseUniqKeys = parseJsFile(filePath, START_JSON_OBJECT)
    jsKeys = [...jsKeys, ...parseUniqKeys];
  };

  findHtmlFiles(INPUT_FOLDER_PATH, findHTMLCallback);
  findJsFiles(INPUT_FOLDER_PATH, findJSCallback);
  const objectToJson = arr => {
    return [...new Set(arr)].reduce((acc, key) => {
      acc[key] = key;
      return acc;
    }, {});
  }

  const groupObject = {
    html: objectToJson(htmlKeys),
    htmlVariable: objectToJson(htmlVar),
    htmlNgBind: objectToJson(htmlNgBind),
    htmlOnlyString: objectToJson(htmlOnlyString),
    js: objectToJson(jsKeys),
  }

  fs.writeFileSync(OUTPUT_JSON_FILE_PATH, JSON.stringify(groupObject, null, 4), 'utf8');
}

main();
