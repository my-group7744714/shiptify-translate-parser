# Shiptify Translate Parser

This is a Node.js script that parses HTML files and extracts translation keys wrapped in double curly braces `{{}}` and `| translate` filters. It then generates a JSON file containing these keys and their values. Keys are extracted from HTML files in a specified folder and are checked for uniqueness before being added to the JSON file.

## Prerequisites

Before running the script, make sure you have Node.js and npm (Node Package Manager) installed on your machine.

## Installation

1. Clone this repository to your local machine.
2. Navigate to the project directory.
3. Run the following command to install dependencies:

```npm
npm install
```


## Usage

1. Create a folder with your HTML and JavaScript files that contain translation keys in the format `{{'key' | translate}}` or `translate('key')`.
2. Configure the script by setting the following environment variables in a `.env` file:

    - `INPUT_FOLDER_PATH`: Path to the folder containing your HTML files.
    - `JSON_FILE_PATH`: Path to the JSON file where the extracted keys and values will be stored.
    - `OUTPUT_JSON_FILE_PATH`: Path to the output JSON file.

3. Run the script using the following command:

```npm
npm start
```

4. The script will process the HTML files, extract unique translation keys, and generate a JSON file with the keys and their values.

## Example

Suppose you have an HTML file with the following content:

```html
<drop-down
  ng-if="ctrl.isShipper"
  dd-search="true"
  dd-placeholder="{{'All Bookers' | translate }}"
  dd-model="ctrl.parent_bookers"
  dd-filter-title="{{'Search bookers' | translate}}"
  dd-change="ctrl.apply_filter()">
</drop-down>
```
And a JavaScript file with the following content:
```javascript
const label = translate('Warehouse');
```
The script will extract the following keys and add them to the JSON file:
```json
{
  "All Bookers": "All Bookers",
  "Search bookers": "Search bookers"
}
```

