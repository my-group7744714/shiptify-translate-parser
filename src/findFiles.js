import fs from "fs";
import path from "path";

export const findHtmlFiles = (dir, callback) => {
  fs.readdirSync(dir).forEach(file => {
    const fullPath = path.join(dir, file);

    if (fs.lstatSync(fullPath).isDirectory()) {
      findHtmlFiles(fullPath, callback);
    } else if (path.extname(fullPath) === '.html') {
      callback(fullPath);
    }
  });
};

export const findJsFiles = (dir, callback) => {
  fs.readdirSync(dir).forEach(file => {
    const fullPath = path.join(dir, file);

    if (fs.lstatSync(fullPath).isDirectory()) {
      findJsFiles(fullPath, callback);
    } else if (path.extname(fullPath) === '.js') {
      callback(fullPath);
    }
  });
};
